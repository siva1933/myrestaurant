import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';
import React from 'react';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  dense: {
    marginTop: 16,
  },
  menu: {
    width: 200,
  },
});


class OutlinedTextFields extends React.Component {

  render() {
    const { classes } = this.props;

    return (
      <form className={classes.container} noValidate autoComplete="off">
        {this.props.multiline ? <TextField
          id="outlined-email-input"
          label={this.props.label}
          className={classes.textField}
          type={this.props.type}
          name={this.props.name}
          value={this.props.value}
          multiline
          margin="normal"
          variant="outlined"
          onChange={this.props.handleChange}
        /> : <TextField
            id="outlined-email-input"
            label={this.props.label}
            className={classes.textField}
            type={this.props.type}
            value={this.props.value}
            name={this.props.name}
            margin="normal"
            variant="outlined"
            onChange={this.props.handleChange}
          />}
      </form>
    );
  }
}

OutlinedTextFields.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(OutlinedTextFields);
