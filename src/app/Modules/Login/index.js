import React from 'react';
import TextField from '../../MaterialComponents/TextField';
import { Card } from '@material-ui/core';
import { Button } from '@material-ui/core';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isNewUser: false
    };
  }

  handleChange = name => e => {
    this.setState({
      [name]: e.target.value
    })
  }

  render() {
    const { isNewUser } = this.state
    return (
      <Card className={'login_body'}>
        {!isNewUser ? <div>
          <TextField type='email' name='email' handleChange={this.handleChange} label='Email' value={this.state.email} />
          <TextField type='password' name='password' handleChange={this.handleChange} label='Password' value={this.state.password} />
          <div>
            <Button size='small'>
              Login
            </Button>
            <Button size='small' onClick={() => { this.setState({ isNewUser: true }) }}>
              New User
            </Button>
          </div>
        </div> : <div>
            <TextField type='email' name='email' handleChange={this.handleChange} label='Email' value={this.state.email} />
            <TextField type='mobile' name='mobile' handleChange={this.handleChange} label='Mobile' value={this.state.mobile} />
            <TextField type='address' name='address' handleChange={this.handleChange} label='Address' multiline={true} value={this.state.address} />
            <TextField type='password' name='password' handleChange={this.handleChange} label='Password' value={this.state.password} />
            <TextField type='password' name='confirmPassword' handleChange={this.handleChange} label='Confirm Password' value={this.state.confirmPassword} />
            <div>
              <Button size='small'>
                Register
            </Button>
              <Button size='small' onClick={() => {
                this.setState({ email: '', password: '', address: '', confirmPassword: '', mobile: '' })
              }}>
                Reset
            </Button>
              <Button onClick={() => { this.setState({ isNewUser: false }) }}>Back to Login</Button>
            </div>
          </div>}
      </Card>
    );
  }
}

export default Login
