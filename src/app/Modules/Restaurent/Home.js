import React from 'react';
import './styles/Home.css';
import Sidebar from '../Sidebar/Sidebar'
import TopBar from '../TopBar/TopBar'


export default class Home extends React.Component {
    render() {
        return (
            <div>
                <TopBar />
                <Sidebar />
            </div>
        );
    }
}
