import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Button } from '@material-ui/core';
import './TopBar.css'
const styles = theme => ({
	margin: {
		margin: theme.spacing.unit,
	},
});

class TopBar extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		const { classes } = this.props;
		return (
			<div className='topbar'>
				<div>
					<Button size="small" className={classes.margin}>
						<label className='button_text_color'>Home</label>
					</Button>
					<Button size="small" className={classes.margin}>
						<label className='button_text_color'>Restaurent</label>
					</Button>
					<Button size="small" className={classes.margin}>
						<label className='button_text_color'>Menu</label>
					</Button>
					<Button size="small" className={classes.margin}>
						<label className='button_text_color'>Order Online</label>
					</Button>
					<Button size="small" className={classes.margin}>
						<label className='button_text_color'>Team</label>
					</Button>
					<Button size="small" className={classes.margin}>
						<label className='button_text_color'>Contact</label>
					</Button>
				</div>
			</div>
		)
	}
}

TopBar.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TopBar);
