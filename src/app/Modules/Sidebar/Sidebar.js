import React from 'react';
import './Sidebar.css';
import Header from '../Header/Header';
import Login from '../Login';

export default class Sidebar extends React.Component {

    render() {
        return (
            <div className="col side_bar">
                <Header />
                <Login />
            </div>
        );
    }
}
