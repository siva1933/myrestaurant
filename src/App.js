import React, { Component } from 'react';
import './App.css';
import { Switch, Route } from 'react-router-dom';
import Home from './app/Modules/Restaurent/Home';

export default class App extends Component {

  render() {
    const routes = (
      <Switch>
        <Route path="/" component={Home} />
      </Switch>);
    return (
          <div className="App">
            {routes}
          </div>
    );
  }
}